<?php
declare(strict_types=1);
include_once "./src/Re.php";

$re=new \Reven\Result\Re(true,"",['a'=>1,'b'=>2]);

$re->setCode("99");
$re->set('c',3);
$re->sets(['d'=>4]);

$re->unset('a','b','c')->setLd(10);

var_dump($re);